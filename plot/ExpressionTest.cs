// ExpressionTest.cs - 动态生成数学表达式并计算其值的测试程序
// 编译方法: csc ExpressionTest.cs Expression.cs

using System;
using Skyiv.Util;

namespace Skyiv.Test
{
  class ExpressionTest
  {
    static void Main(string [] args)
    {
      try
      {
        if (args.Length > 0)
        {
          Console.WriteLine("f(x): {0}", args[0]);
          Expression expression = new Expression(args[0]);
          for (int i = 1; i < args.Length; i++)
          {
            double x = double.Parse(args[i]);
            Console.WriteLine("f({0}) = {1}", x, expression.Compute(x));
          }
        }
        else Console.WriteLine("Usage: ExpressionTest expression [ parameters  ]");
      }
      catch (Exception ex)
      {
        Console.WriteLine("错误: " + ex.Message);
      }
    }
  }
}

